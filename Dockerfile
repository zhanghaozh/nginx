FROM nginx:1.21.0

ADD nginx.conf /etc/nginx/

ADD conf.d /etc/nginx/conf.d

EXPOSE 80