## 部署新服务

修改./conf.d/app.location 在其中添加新服务的路由

服务名字前需添加docker stack的前缀“devops_"

如

```shell
set $test http://devops_test;

location / {
    proxy_pass $test;
}
```



